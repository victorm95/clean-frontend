import { useEffect, useState } from "react";
import { GetAllToDosUseCase } from "todo-usecase";
import { ToDoRepository } from "todo-infrastructure-repositories";
import { ToDoRestAdapterImpl } from "todo-infrastructure-ky";

const adapter = new ToDoRestAdapterImpl();
const gateway = new ToDoRepository(adapter);
const usecase = new GetAllToDosUseCase(gateway);

function App() {
  const [todos, setTodos] = useState([]);

  useEffect(() => {
    usecase.execute().then(setTodos);
  }, []);

  return <div></div>;
}

export default App;
