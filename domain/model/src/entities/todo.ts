export class ToDo {
  constructor(public id: string,
             public name: string,
             public completed: boolean) {}
}
