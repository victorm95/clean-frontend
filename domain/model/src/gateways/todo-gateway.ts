import { ToDo } from '../entities/todo'

export interface ToDoGateway {
  createToDo(todo: ToDo): Promise<ToDo>
  getAllToDos(): Promise<ToDo[]>
  checkToDo(id: string, checked: boolean): Promise<ToDo>
}
