import { ToDo, ToDoGateway } from 'todo-model'

export class CheckToDoUseCase {
  constructor(private gateway: ToDoGateway) {}

  execute(id: string, checked: boolean): Promise<ToDo> {
    return this.gateway.checkToDo(id, checked)
  }
}
