import { ToDo, ToDoGateway } from 'todo-model'

export class CreateToDoUseCase {
  constructor(private gateway: ToDoGateway) {}

  async execute(todo: ToDo): Promise<ToDo> {
    if (!todo.name) {
      throw new Error("The ToDo's name is required")
    }

    return await this.gateway.createToDo(todo)
  }
}
