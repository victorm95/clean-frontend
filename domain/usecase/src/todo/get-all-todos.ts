import { ToDo, ToDoGateway } from 'todo-model'

export class GetAllToDosUseCase {
  constructor(private gateway: ToDoGateway) {}

  execute(): Promise<ToDo[]> {
    return this.gateway.getAllToDos()
  }
}
