import { ToDo, ToDoGateway } from 'todo-model'

export interface ToDoRestAdapter {
  post(todo: ToDo): Promise<ToDo>
  getAll(): Promise<ToDo[]>
  get(id: string): Promise<ToDo | undefined>
  put(id: string, todo: ToDo): Promise<ToDo>
}
