import ky from 'ky'
import { ToDo } from 'todo-model'
import { ToDoRestAdapter } from 'todo-infrastructure-adapters'

export class ToDoRestAdapterImpl implements ToDoRestAdapter {
  post(todo: ToDo): Promise<ToDo> {
    return ky.post('http://localhost:7070/todos', { json: todo }).json()
  }

  getAll(): Promise<ToDo[]> {
    return ky.get('http://localhost:7070/todos').json()
  }

  get(id: string): Promise<ToDo> {
    return ky.get(`http://localhost:7070/todos/${id}`).json()
  }

  put(id: string, todo: ToDo): Promise<ToDo> {
    return ky.put(`http://localhost:7070/todos/${id}`, { json: todo }).json()
  }
}
