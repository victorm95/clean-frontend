import { ToDo, ToDoGateway } from 'todo-model'
import { ToDoRestAdapter } from 'todo-infrastructure-adapters'

export class ToDoRepository implements ToDoGateway {
  constructor(private adapter: ToDoRestAdapter) {}

  createToDo(todo: ToDo): Promise<ToDo> {
    return this.adapter.post(todo)
  }

  getAllToDos(): Promise<ToDo[]> {
    return this.adapter.getAll()
  }

  async checkToDo(id: string, checked: boolean): Promise<ToDo> {
    const todo = await this.adapter.get(id)

    if (!todo) {
      throw new Error("ToDo not found")
    }

    todo.completed = checked

    return this.adapter.put(id, todo)
  }
}
